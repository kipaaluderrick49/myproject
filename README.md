# **FarmSelect.**


## **DESCRIPTION:**
The **FarmSelect**is an app to link farmers of different locations with their customers.

This app is to enable farmers of different locations show off farm products ready for sell to consumers of different locations. This is aimed at creating a wide market for farmers products with in the country.

### **USAGE:**
A farmer with goods ready for sell will only required to install the app, provide his contacts and his location on uploading the product.
A given customer with interest in the uploaded product will either call the farmer for more details or make an order from the app.

---
#### **AUHORS:**
| Name  | Contact |
| ---   | --------|
| Derrick |   derrickkipaalu106@gmail.com |

##### **STATUS:**
*The project has just been started off*
